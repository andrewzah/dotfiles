# Cargo / Rust
export PATH="$HOME/.cargo/bin:$PATH"

# rbenv
eval "$(rbenv init -)"

# nvm
source /usr/share/nvm/init-nvm.sh
